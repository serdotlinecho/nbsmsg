if [[ -d "$ZDOTDIR" ]]; then
  for file in "$ZDOTDIR"/{lib,plugins/*}/*.zsh; do
    source "$file"
  done
fi

path=($HOME/.local/bin $path)
export PATH
typeset -U path
