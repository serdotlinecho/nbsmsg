# -------------------------------------------------------------
# file:     ~/.zshrc.alias
# author:   serdotlinecho
# vim:fenc=utf-8:ai:si:et:ts=4:sw=4:
# -------------------------------------------------------------

# directory movement
alias ..='cd ..'
alias ...='../..'
alias ....='../../..'
alias .....='../../../..'

# directory info
alias ls='ls --group-directories-first --color=auto'  # group directories before files
alias la='ls -A'
alias ll='ls -lh'
alias lla='ls -lhA'
alias lsd='ls -d */'
alias dirsize='du -h --max-depth=1 "$@" | sort -k 1,1hr -k 2,2f'

# core
alias cp='vcp -g'
alias mv='vmv -g'
alias rmf='rm -rf'
alias -g L='|less'
alias hgrep='history | grep'
alias dmesg='dmesg --human -T'
alias mgrep='lsmod | grep'
alias tree='tree --dirsfirst'
# alias vim='vim -u "${XDG_CONFIG_HOME}/vim/vimrc"'
alias vdiff='vimdiff'
alias vless='/usr/share/vim/vim74/macros/less.sh'

# net
alias wlist='nmcli -p dev wifi'  # list available wifi
alias localip="ip a s|sed -ne '/127.0.0.1/!{s/^[ \t]*inet[ \t]*\([0-9.]\+\)\/.*$/\1/p}'"
alias globalip='dig +short myip.opendns.com @resolver1.opendns.com'
alias turl="curl --socks5-hostname 127.0.0.1:9050 "
alias fing='fping -ga 192.168.1.0/24 2> /dev/null'
alias netspd='wget -O /dev/null http://cachefly.cachefly.net/100mb.test; true'
alias lstr='livestreamer'
alias aria2c='aria2c --dht-file-path=$XDG_CACHE_HOME/aria2/dht.dat'
alias bt="aria2c --conf-path=${XDG_CONFIG_HOME}/aria2/aria2.bittorrent"
alias btinfo="aria2c -S"
alias magnet2torrent='aria2c -q --bt-metadata-only --bt-save-metadata'
alias rtorrent="rtorrent -n -o import=${XDG_CONFIG_HOME}/rtorrent/config.rc"
alias ytdl="youtube-dl"

# random
alias wiki='wiki-search'
alias archey='archey3'
alias almix='alsamixer -c 0'
alias colo='cblocks.sh;colordump.sh'
alias drop_cache='echo 3 | sudo tee /proc/sys/vm/drop_caches'
alias mpvq='mpv --really-quiet'
alias ncmp='ncmpcpp -c $XDG_CONFIG_HOME/ncmpcpp/config'
alias scb='sudo setpci -s 00:02.0 F4.B=70'  # set screen brightness to 70
alias show_path="echo $PATH | tr ':' '\n'"
alias zzz='systemctl suspend'  # suspend the machine
alias :q='exit'

# git
alias g='git'
alias ga='git add'
alias gst='git status -sb'
alias gdt='git difftool'
alias gc='git commit -vm'
alias gp='git push'
alias gf='git fetch'

# suffix
alias -s {mp4,webm,avi}=mpv
alias -s {jpg,png}=feh
alias -s {txt,srt}=vim
