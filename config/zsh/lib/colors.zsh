autoload -U colors
colors

if [ -n "$DISPLAY" ]; then
    eval $(dircolors -b ~/.dircolors.tango.dark)
fi

# Enable color in grep
export GREP_COLOR='1;33'

# solarized colors man page
export LESS_TERMCAP_mb=$'\E[01;32m'
export LESS_TERMCAP_md=$'\E[0;34m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;30;43m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[0;31m'

# linux console colors (jwr dark)
# if [ "$TERM" = "linux" ]; then
#     echo -en "\e]P0000000" #black
#     echo -en "\e]P85e5e5e" #darkgrey
#     echo -en "\e]P18a2f58" #darkred
#     echo -en "\e]P9cf4f88" #red
#     echo -en "\e]P2287373" #darkgreen
#     echo -en "\e]PA53a6a6" #green
#     echo -en "\e]P3914e89" #darkyellow
#     echo -en "\e]PBbf85cc" #yellow
#     echo -en "\e]P4395573" #darkblue
#     echo -en "\e]PC4779b3" #blue
#     echo -en "\e]P55e468c" #darkmagenta
#     echo -en "\e]PD7f62b3" #magenta
#     echo -en "\e]P62b7694" #darkcyan
#     echo -en "\e]PE47959e" #cyan
#     echo -en "\e]P7899ca1" #lightgrey
#     echo -en "\e]PFc0c0c0" #white
#     clear # bring us back to default input colours
# fi
