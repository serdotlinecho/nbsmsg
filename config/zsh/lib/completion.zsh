autoload -U compinit
compinit
zmodload -i zsh/complist

# Case-insensitive (all),partial-word and then substring completion
if [ "x$CASE_SENSITIVE" = "xtrue" ]; then
    zstyle ':completion:*' matcher-list 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
    unset CASE_SENSITIVE
else
    zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
fi

zstyle ':completion:*' menu select=2
zstyle ':completion:*' file-sort modification reverse
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*:aliases' list-colors "=*=$color[green]"
zstyle '*:processes-names' command 'ps -e -o comm='
zstyle :compinstall filename '$XDG_CONFIG_HOME/zsh/.zshrc'

# Speed up zsh completion
zstyle ':completion:*' accept-exact '*(N)'
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.config/zsh/cache
