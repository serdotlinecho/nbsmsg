# ----------------------------------------------------
# file:     ~/.config/zsh/lib/functions.zsh
# vim:fenc=utf-8:ai:si:et:ts=4:sw=4:fdm=marker:ft=sh:
# ----------------------------------------------------

# ----- config files {{{
cfg-zsh() { $EDITOR $ZDOTDIR/.zshrc && source $ZDOTDIR/.zshrc ;}
    rld-zsh() { source $ZDOTDIR/.zshrc ;}
cfg-zalias() { $EDITOR $ZDOTDIR/lib/aliases.zsh && source $ZDOTDIR/.zshrc ;}
cfg-zfunctions() { $EDITOR $ZDOTDIR/lib/functions.zsh && source $ZDOTDIR/.zshrc ;}
cfg-zcolors() { $EDITOR $ZDOTDIR/lib/colors.zsh && source $ZDOTDIR/.zshrc ;}
cfg-zoptions() { $EDITOR $ZDOTDIR/lib/setopt.zsh && source $ZDOTDIR/.zshrc ;}
cfg-zbindings() { $EDITOR $ZDOTDIR/lib/key-bindings.zsh && source $ZDOTDIR/.zshrc ;}
cfg-zcompletions() { $EDITOR $ZDOTDIR/lib/completion.zsh && source $ZDOTDIR/.zshrc ;}
cfg-zexport() { $EDITOR $ZDOTDIR/lib/exports.zsh && source $ZDOTDIR/.zshrc ;}
cfg-zprompt() { $EDITOR $ZDOTDIR/lib/prompt.zsh && source $ZDOTDIR/.zshrc ;}
cfg-zprofile() { $EDITOR $ZDOTDIR/.zprofile ;}
cfg-zenv() { $EDITOR ~/.zshenv && source $ZDOTDIR/.zshrc ;}
cfg-i3() { $EDITOR ~/.config/i3/config ;}
cfg-i3status() { $EDITOR ~/.config/i3status/config ;}
cfg-i3pystatus() { $EDITOR ~/.local/bin/i3pystatus_.py ;}
cfg-mpd() { $EDITOR ~/.config/mpd/mpd.conf ;}
cfg-mplayer() { $EDITOR ~/.config/mplayer/config ;}
cfg-mpv() { $EDITOR ~/.config/mpv/config ;}
cfg-ncmp() { $EDITOR ~/.config/ncmpcpp/config ;}
cfg-newsbeuter() { $EDITOR ~/.config/newsbeuter/config ;}
cfg-dunst() { $EDITOR ~/.config/dunst/dunstrc ;}
cfg-ranger() { $EDITOR ~/.config/ranger/rc.conf ;}
cfg-rtorrent() { $EDITOR ~/.config/rtorrent/config.rc ;}
cfg-tmux() { $EDITOR ~/.tmux.conf ;}
    rld-tmux() { tmux source-file ~/.tmux.conf ;}
cfg-vim() { $EDITOR ~/.config/vim/vimrc ;}
cfg-cvim() { $EDITOR ~/dotfiles/config/cvimrc ;}
cfg-vimpr() { $EDITOR ~/.config/vimperator/vimperatorrc ;}
cfg-xinitrc() { $EDITOR ~/.xinitrc ;}
cfg-xresources() { $EDITOR ~/.Xresources && xrdb -load ~/.Xresources ;}
    rld-xresources() { xrdb -load ~/.Xresources ;}
cfg-compton() { $EDITOR ~/.config/compton.conf ;}
cfg-turses() { $EDITOR ~/.turses/config ;}
cfg-youtubeviewer() { $EDITOR ~/.config/youtube-viewer/youtube-viewer.conf ;}

# }}}
# ----- Arch Linux pac-helper {{{
paclog() { tail -n"$1" /var/log/pacman.log ;}
pacmod() { pacman -Qii $@ | awk '/^MODIFIED/ {print $2}' ;}
pacnew() { egrep "pac(new|orig|save)" /var/log/pacman.log ;}
pactimize() { sudo pacman-optimize && sync ;}

# }}}
# ----- Youtube {{{
# stream youtube video
vlc-ytsmall() { vlc --no-video-title-show $(xsel -ob)\&fmt\=05 vlc://quit ;}
vlc-ytmed() { vlc --no-video-title-show $(xsel -ob)\&fmt\=18 vlc://quit ;}
mpv2c() { mpv --ytdl-format=5 $(xsel -ob) ;}
mpv3c() { mpv --ytdl-format=18 $(xsel -ob) ;}

# download youtube video
ytdl2c() { youtube-dl -f 5 -o '%(title)s.%(ext)s' $(xsel -ob) ;}
ytdl3c() { youtube-dl -f 18 -o '%(title)s.%(ext)s' $(xsel -ob) ;}

# youtube-viewer
ytv() { clear && figlet -c youtube viewer && youtube-viewer $@ ;}
ytv-subscriptions() { clear && figlet -c my subscriptions && youtube-viewer -S ;}
# youtube channel
# gotbletu
ytc-gotbletu() { clear && figlet -c gotbletu && youtube-viewer --results 10 -u gotbletu ;}
# jupiter Broadcasting
ytc-jbcast() { clear && figlet -c jupiter broadcasting && youtube-viewer --results 10 -u jupiterbroadcasting ;}
# linux4unme
ytc-linux4unme() { clear && figlet -c linux4unme && youtube-viewer --results 10 -u linux4unme ;}
# yu-Jie Lin
ytc-livibetter() { clear && figlet -c yu jie lin && youtube-viewer --results 10 -u livibetter ;}
# kris Occhipinti
ytc-metalx1000() { clear && figlet -c metalx1000 && youtube-viewer --results 10 -u metalx1000 ;}

# }}}
# ----- Net {{{
# paste to sprunge
sprung() { curl -F "sprunge=<-" http://sprunge.us <"$1" ;}

# quickly look up HTTP response codes
function http() {
    curl http://httpcode.info/$1
}

# my computer can speak!
function say() {
    mplayer -really-quiet "http://translate.google.com/translate_tts?tl=en&q=$*"|sed 's/ /+/3g'|sh 2>/dev/null;
}

function kata() {
    mplayer -really-quiet "http://translate.google.com/translate_tts?tl=id&q=$*"|sed 's/ /+/3g'|sh 2>/dev/null;
}

# capture rtmp links
sniff-begin() { sudo iptables -t nat -A OUTPUT -p tcp --dport 1935 -j REDIRECT ;}
sniff-capture-rtmpsrv() { rtmpsrv ;}
sniff-end() { sudo iptables -t nat -D OUTPUT -p tcp --dport 1935 -j REDIRECT ;}

# open a random pirate bay mirror site
pirate-proxy() {
    url=`curl http://proxybay.info/ | awk -F'href="|"  |">|</' '{for(i=2;i<=NF;i=i+4) print $i,$(i+2)}' |\
    grep follow|sed 's/^.\{19\}//'|shuf -n 1` && dwb $url;
}

# }}}
# ----- Custom {{{
# Create and enter directory
mkdcd() {
    mkdir -p "$@" && cd "$1"
}

ping() { grc --colour=auto /usr/bin/ping "$@" }

udisksctl() {
    [[ $1 = unmount ]] && sync
    command udisksctl $@
}

extract() {
  if [ -f $1 ] ; then
      case $1 in
          *.tar.bz2)   tar xvjf $1    ;;
          *.tar.gz)    tar xvzf $1    ;;
          *.tar.xz)    tar xvJf $1    ;;
          *.bz2)       bunzip2 $1     ;;
          *.rar)       unrar x $1     ;;
          *.gz)        gunzip $1      ;;
          *.tar)       tar xvf $1     ;;
          *.tbz2)      tar xvjf $1    ;;
          *.tgz)       tar xvzf $1    ;;
          *.zip)       unzip $1       ;;
          *.Z)         uncompress $1  ;;
          *.7z)        7z x $1        ;;
          *.xz)        unxz $1        ;;
          *.exe)       cabextract $1  ;;
          *)           echo "don't know how to extract '$1'..." ;;
      esac
  else
      echo "'$1' is not a valid file!"
  fi
}

# set the cpu frequency scaling governor
setgov() {
    echo "$1" | sudo tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor
}

# find out all available kernel modules
fkm() {
  local kver=$(uname -r) arg=${1//[-_]/[-_]}
  find "/lib/modules/$kver" -iname "*$arg*.ko*" \
    -exec bash -c 'mods=("${@##*/}"); printf "%s\n" "${mods[@]%.ko*}"' _ {} +

  if [[ ! -e /lib/modules/$kver/kernel ]]; then
    echo "reboot!" >&2
  fi
}

# python calculator, press Ctrl+D to quit
calc() { python -ic "from __future__ import division; from math import *; from random import *" ;}

# show FN keys
fnkeys() { xev | grep -A2 --line-buffered '^KeyRelease' | sed -n '/keycode /s/^.*keycode \([0-9]*\).* (.*, \(.*\)).*$/\1 \2/p'}

# }}}
