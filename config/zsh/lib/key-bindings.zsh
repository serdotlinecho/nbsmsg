# To see the key combo you want to use just do:
# cat > /dev/null
# And press it

# Default to standard vi bindings, regardless of editor string
bindkey -v
KEYTIMEOUT=1

bindkey '^U' kill-whole-line
bindkey -M viins "^A" vi-beginning-of-line
bindkey -M viins "^E" vi-end-of-line
bindkey -a '^Y' redo
bindkey -M vicmd 'u' undo

autoload -U history-search-end
zle -N history-beginning-search-backward-end history-search-end
zle -N history-beginning-search-forward-end history-search-end

bindkey "^[[A" history-beginning-search-backward-end
bindkey "^[[B" history-beginning-search-forward-end
bindkey '^R' history-incremental-search-backward
bindkey -M vicmd '^R' history-incremental-pattern-search-backward

# use the vi navigation keys in menu completion
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
