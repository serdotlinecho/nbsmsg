setopt prompt_subst
autoload colors && colors

local prefix="%{$fg[default]%}─(%{$reset_color%}"
local suffix="%{$fg[default]%})%{$reset_color%}"
local user_host="%{$fg_bold[magenta]%}%n@%m%{$reset_color%}"
local current_dir="%{$fg_bold[blue]%}%~%{$reset_color%}"
local git_branch='$(_git_prompt_info)%{$reset_color%}'

# git info
_git_prompt_info() {
    local _hash=$(git show -s --pretty=format:%h HEAD 2> /dev/null)
    [[ -z $_hash ]] && return
    local _name=$(git symbolic-ref --short HEAD 2> /dev/null)
    [[ -n $_name ]] && _name="$_name"
    echo -n "${prefix}%{$fg_bold[yellow]%}${_name}-${_hash}%{$reset_color%}${suffix}"
}

# Git Prompt
# _git_prompt_info() {
#   ref=$(git symbolic-ref HEAD 2> /dev/null) || return
#   echo "─(%{$fg[yellow]%}${ref#refs/heads/}%{$reset_color%})"
# }

function ssh_connection() {
    if [[ -n $SSH_CONNECTION ]]; then
        echo "${prefix}%{$fg_bold[red]%}ssh%{$reset_color%}${suffix}"
    fi
}

PROMPT="┌─${prefix}${user_host}${suffix}$(ssh_connection)${prefix}${current_dir}${suffix}${git_branch}
└─%{$reset_color%} "

# ZSH_THEME_GIT_PROMPT_SUFFIX=""
# ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[red]%}*%{$reset_color%}"

# Change cursor to red in vi normal mode
# urxvt (and family) accepts even #RRGGBB
INSERT_PROMPT="gray"
COMMAND_PROMPT="red"

# helper for setting color including all kinds of terminals
set_prompt_color() {
    if [[ $TERM = "linux" ]]; then
       # nothing
    elif [[ $TMUX != '' ]]; then
        printf '\033Ptmux;\033\033]12;%b\007\033\\' "$1"
    else
        echo -ne "\033]12;$1\007"
    fi
}

# change cursor color basing on vi mode
zle-keymap-select () {
    if [ $KEYMAP = vicmd ]; then
        set_prompt_color $COMMAND_PROMPT
    else
        set_prompt_color $INSERT_PROMPT
    fi
}

zle-line-finish() {
    set_prompt_color $INSERT_PROMPT
}

zle-line-init () {
    zle -K viins
    set_prompt_color $INSERT_PROMPT
}

zle -N zle-keymap-select
zle -N zle-line-init
zle -N zle-line-finish
